package ru.tsc.apozdnov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.tsc.apozdnov.tm.api.service.IConnectionService;
import ru.tsc.apozdnov.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.apozdnov.tm.dto.model.ProjectDTO;
import ru.tsc.apozdnov.tm.exception.field.EmptyDescriptionException;
import ru.tsc.apozdnov.tm.exception.field.EmptyNameException;
import ru.tsc.apozdnov.tm.exception.field.EmptyUserIdException;
import ru.tsc.apozdnov.tm.repository.dto.ProjectRepositoryDTO;

import javax.persistence.EntityManager;
import java.util.Date;

public class ProjectServiceDTO extends AbstractUserOwnedServiceDTO<ProjectDTO, IProjectRepositoryDTO>
        implements IProjectServiceDTO {

    public ProjectServiceDTO(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected IProjectRepositoryDTO getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectRepositoryDTO(entityManager);
    }

    @Nullable
    @Override
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setUserId(userId);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Nullable
    @Override
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Nullable
    @Override
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

}