package ru.tsc.apozdnov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectUpdateByIdResponse extends AbstractProjectResponse {

    public ProjectUpdateByIdResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

}